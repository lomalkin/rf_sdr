#!/usr/bin/env python

import time
import serial
import fileinput

ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

ser.isOpen()

input=1
while input :
    data = ""
    for line in fileinput.input():
        data += line # + "\n"
    
    print data
    print "Sending...\n"
    ser.write(data)
    print "Done\n"
    input = 0
    time.sleep(1)
