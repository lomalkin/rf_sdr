#!/usr/bin/env python

import time
import serial

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

ser.isOpen()

flag=1
while flag :
    #with open ("data.txt", "r") as myfile:
    #    #ser.write(myfile.readlines())
    #    data = myfile.readlines()
    data = open('data_radio9600.txt', 'r').read()
    
    #out += ser.read(1)
    print data
    print "Sending...\n"
    ser.write(data)
    print "Done\n"
    flag = 0
    
    time.sleep(1)

