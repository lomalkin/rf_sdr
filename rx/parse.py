#!/usr/bin/env python

BIT_COUNT = 10
BIT_SIZE = 54

def sym_proc (sym, ctx):
	ctx["char"] = None
	
	sym = (1 if sym == '\x01' else 0)
	'''
	if sym == 1:
		ctx["char"] = "1"
		print 1
	else:
		ctx["char"] = None
		print 0
	'''
	
	if ((ctx["frame_counter"]) % BIT_SIZE) == 0 and ctx["frame_counter"] > 0:
		ctx["frame"] += sym << (BIT_COUNT - (ctx["frame_counter"]) / BIT_SIZE)
		print "->", sym
		
	if ctx["frame_counter"] == 0 and ctx["prev"] == 0 and sym == 1:
		ctx["frame_counter"] = BIT_COUNT * BIT_SIZE - (3*BIT_SIZE/4)
		print ""
		
	if ctx["frame_counter"] > 0:
		ctx["frame_counter"] -= 1
		print sym,
		
	if ctx["frame_counter"] == 0 and ctx["frame"] != 0:
		ctx["char"] = ctx["frame"]
		ctx["frame"] = 0
		print "[frame end]"
	
	ctx["prev"] = sym
	
	return ctx

def reverse_bit(num):
	result = 0
	while num:
		result = (result << 1) + (num & 1)
		num >>= 1
	return result

def main():
	file = "in.bin"
	ctx = {
		"char":None,
		"prev": 0,
		"frame_counter": 0,
		"frame": 0
	}
	fs = 48000
	br = 9600
	
	f = open(file, "rb")
	try:
		byte = f.read(1)
		while byte != "":
			ctx = sym_proc(byte, ctx)
			if ctx["char"]:
				#res_char = (ctx["char"] >> 1) & 0xFF
				res_char = (ctx["char"] >> 1)  & 0xFF
				#res_char = reverse_bit(res_char)
				if res_char != 0:
					#print repr(chr(res_char)), # "%c %0X" % (res_char, res_char),
					print chr(res_char),
					print hex(res_char), # "%c %0X" % (res_char, res_char),
					
			byte = f.read(1)
	finally:
		f.close()

main()
