#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Top Block
# Generated: Tue Dec 12 01:36:00 2017
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import blks2 as grc_blks2
from optparse import OptionParser
import osmosdr
import time


class top_block(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Top Block")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 96000
        self.data_sample_rate = data_sample_rate = 48000

        ##################################################
        # Blocks
        ##################################################
        self.osmosdr_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + "" )
        self.osmosdr_source_0.set_sample_rate(samp_rate)
        self.osmosdr_source_0.set_center_freq(433905000, 0)
        self.osmosdr_source_0.set_freq_corr(0, 0)
        self.osmosdr_source_0.set_dc_offset_mode(2, 0)
        self.osmosdr_source_0.set_iq_balance_mode(0, 0)
        self.osmosdr_source_0.set_gain_mode(False, 0)
        self.osmosdr_source_0.set_gain(10, 0)
        self.osmosdr_source_0.set_if_gain(20, 0)
        self.osmosdr_source_0.set_bb_gain(20, 0)
        self.osmosdr_source_0.set_antenna("", 0)
        self.osmosdr_source_0.set_bandwidth(0, 0)
          
        self.low_pass_filter_1 = filter.fir_filter_fff(1, firdes.low_pass(
        	1, data_sample_rate, 10, 12, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0 = filter.fir_filter_ccf(1, firdes.low_pass(
        	1, samp_rate, 8000, 8500, firdes.WIN_HAMMING, 6.76))
        self.blocks_threshold_ff_0 = blocks.threshold_ff(0.01, 0.02, 0)
        self.blocks_sub_xx_0 = blocks.sub_ff(1)
        self.blocks_float_to_uchar_0 = blocks.float_to_uchar()
        self.blks2_tcp_sink_0 = grc_blks2.tcp_sink(
        	itemsize=gr.sizeof_char*1,
        	addr="127.0.0.1",
        	port=8123,
        	server=False,
        )
        self.analog_am_demod_cf_0 = analog.am_demod_cf(
        	channel_rate=data_sample_rate,
        	audio_decim=samp_rate/data_sample_rate,
        	audio_pass=5000,
        	audio_stop=5500,
        )

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_am_demod_cf_0, 0), (self.blocks_sub_xx_0, 0))    
        self.connect((self.analog_am_demod_cf_0, 0), (self.low_pass_filter_1, 0))    
        self.connect((self.blocks_float_to_uchar_0, 0), (self.blks2_tcp_sink_0, 0))    
        self.connect((self.blocks_sub_xx_0, 0), (self.blocks_threshold_ff_0, 0))    
        self.connect((self.blocks_threshold_ff_0, 0), (self.blocks_float_to_uchar_0, 0))    
        self.connect((self.low_pass_filter_0, 0), (self.analog_am_demod_cf_0, 0))    
        self.connect((self.low_pass_filter_1, 0), (self.blocks_sub_xx_0, 1))    
        self.connect((self.osmosdr_source_0, 0), (self.low_pass_filter_0, 0))    

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 8000, 8500, firdes.WIN_HAMMING, 6.76))
        self.osmosdr_source_0.set_sample_rate(self.samp_rate)

    def get_data_sample_rate(self):
        return self.data_sample_rate

    def set_data_sample_rate(self, data_sample_rate):
        self.data_sample_rate = data_sample_rate
        self.low_pass_filter_1.set_taps(firdes.low_pass(1, self.data_sample_rate, 10, 12, firdes.WIN_HAMMING, 6.76))


def main(top_block_cls=top_block, options=None):

    tb = top_block_cls()
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
